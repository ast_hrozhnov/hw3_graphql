import defaultExport from 'apollo-datasource-rest';
const { RESTDataSource } = defaultExport;

export default class SpacexAPI extends RESTDataSource {
    constructor() {
      super();
      this.baseURL = 'https://api.spacexdata.com/v3';
    }

    async getDragon(id) {
        return this.get(`dragons/${id}`);
    }

    async getDragons() {
        return this.get(`dragons`);
    }
}