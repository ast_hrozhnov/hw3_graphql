Dragons:
```
query {
  dragons {
  	name
    description
    wikipedia
    thrusters {
      type
      amount
      pods
    }

  }
}
```

Dragon:
```
query getDragon {
  dragon(id: "dragon1") {
    name
    description
    wikipedia
    thrusters {
      type
      amount
      pods
    }
  }
}
```